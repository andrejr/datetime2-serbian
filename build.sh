#!/usr/bin/env bash

FULL_PATH=$(readlink -f "$0")
SCRIPT_FULL_DIR=$(dirname "$FULL_PATH")

PACKAGE=${SCRIPT_FULL_DIR##*/}
LATEX="lualatex -interaction nonstopmode -halt-on-error -file-line-error"
LATEXMK=latexmk

PACKAGE_LATEX="$HOME/texmf/tex/latex/$PACKAGE"
PACKAGE_DOC="$HOME/texmf/doc/latex/$PACKAGE"

function build() {
  ./generate_package.py &&
    $LATEXMK "$PACKAGE.dtx" &&
    $LATEX "$PACKAGE.ins"
}

if [ "$1" == "clean" ]; then
  rm -rfv "$PACKAGE_LATEX" "$PACKAGE_DOC"
elif [ "$1" == "copylocal" ]; then
  latexmk -C "$PACKAGE.dtx" &&
    build | grep "Checksum passed" &&
    rm -rf "$PACKAGE_LATEX" "$PACKAGE_DOC" &&
    mkdir -pv "$PACKAGE_LATEX" &&
    cp -vf ./*.{sty,def,cls,cfg,ldf} "$PACKAGE_LATEX/" &&
    mkdir -pv "$PACKAGE_DOC" &&
    cp -vf ./*.pdf "$PACKAGE_DOC"
elif [ "$1" == "ctan" ]; then
  rm -rf "$PACKAGE"
  rm -rf "$PACKAGE.zip"
  latexmk -C "$PACKAGE.dtx" &&
    build | grep "Checksum passed" &&
    mkdir "$PACKAGE" &&
    cp "$PACKAGE.ins" "$PACKAGE.dtx" "$PACKAGE.pdf" README.md "$PACKAGE/" &&
    zip -r "$PACKAGE.zip" "$PACKAGE/"
elif [ -z "$1" ]; then
  build
else
  echo "Unknown argument \"$1\"." 1>&2
  exit 1
fi
