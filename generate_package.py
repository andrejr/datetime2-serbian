#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from typing import List
from typing import Union

import yaml
from jinja2 import Environment
from jinja2 import FileSystemLoader
from srtools import latin_to_cyrillic

from utf8_to_licr import translation_table as licr_translation_dict

VERSIONS = {
    "2.0.0": "2019-11-07",
    "2.0.1": "2019-11-11",
    "2.1.0": "2019-11-22",
}


def latest_version(parentheses: bool = False) -> str:
    version = max(VERSIONS.keys())
    date = VERSIONS[version]
    if parentheses:
        return f"{date} (v{version})"
    return f"{date.replace('-', '/')} v{version}"


def changes(version_number: float) -> str:
    date = VERSIONS[version_number]
    return f"\\changes{{{version_number}}}{{{date}}}"


latex_env = Environment(
    block_start_string=r"\BLOCK{",
    block_end_string=r"}",
    variable_start_string=r"\VAR{",
    variable_end_string=r"}",
    comment_start_string=r"\#{",
    comment_end_string=r"}",
    line_statement_prefix=r"%%!",
    line_comment_prefix=r"%#",
    trim_blocks=True,
    lstrip_blocks=True,
    autoescape=False,
    loader=FileSystemLoader(os.path.abspath("templates")),
)
latex_env.globals["latest_version"] = latest_version
latex_env.globals["changes"] = changes


def macro_name(text: str) -> str:
    return "".join(char for char in text if char.isalpha())


latex_env.filters["macro_name"] = macro_name


def words_localization(
    script: str,
    scr_sh: str,
    language: str,
    lang_sh: str,
    months: List[str],
    letter_i: str,
    weekdays_ek: List[str],
    weekdays_ij: List[str],
) -> str:
    template = latex_env.get_template(
        "single_script_localization_template.dtx"
    )
    return template.render(**locals())


def generate_word_localizations() -> str:

    with open("localization_strings.yaml") as ymlfile:
        latin_localization_strings = yaml.load(ymlfile, Loader=yaml.FullLoader)

    def convert_to_cyrillic(item: Union[List, str]) -> Union[List, str]:
        if isinstance(item, str):
            return latin_to_cyrillic(item)
        if isinstance(item, List):
            return [latin_to_cyrillic(list_item) for list_item in item]

        raise TypeError("Unknown item type.")

    cyrillic_localization_strings = {
        key: convert_to_cyrillic(item)
        for key, item in latin_localization_strings.items()
    }

    cyrillic_localization = words_localization(
        script="Cyrillic",
        scr_sh="cyr",
        language="Serbian",
        lang_sh="serbian",
        **cyrillic_localization_strings,
    )

    latin_localization = words_localization(
        script="Latin",
        scr_sh="lat",
        language="Serbian",
        lang_sh="serbian",
        **latin_localization_strings,
    )

    return latin_localization + "\n" + cyrillic_localization


def generate_base_package() -> str:
    main_dtx_template = latex_env.get_template("base_package.dtx")
    return main_dtx_template.render(language="Serbian", lang_sh="serbian")


def generate_region(
    region: str, lang_sh: str, script_sh: str, baselang: str, def_pron_sh: str
) -> str:
    srcstr = lang_sh + script_sh
    template = latex_env.get_template("region_template.dtx")
    return template.render(**locals())


def main():
    unicode_localizations = generate_word_localizations()
    ascii_localizations = unicode_localizations.translate(
        licr_translation_dict
    )
    base_package = generate_base_package()

    regions = [
        {
            "region": "serbian",
            "lang_sh": "serbian",
            "script_sh": "lat",
            "baselang": "serbian",
            "def_pron_sh": "ek",
        },
        {
            "region": "sr-Latn",
            "lang_sh": "serbian",
            "script_sh": "lat",
            "baselang": "serbian",
            "def_pron_sh": "ek",
        },
        {
            "region": "sr-Latn-RS",
            "lang_sh": "serbian",
            "script_sh": "lat",
            "baselang": "serbian",
            "def_pron_sh": "ek",
        },
        {
            "region": "sr-Latn-ME",
            "lang_sh": "serbian",
            "script_sh": "lat",
            "baselang": "serbian",
            "def_pron_sh": "ij",
        },
        {
            "region": "sr-Latn-BA",
            "lang_sh": "serbian",
            "script_sh": "lat",
            "baselang": "serbian",
            "def_pron_sh": "ij",
        },
        {
            "region": "serbianc",
            "lang_sh": "serbian",
            "script_sh": "cyr",
            "baselang": "serbianc",
            "def_pron_sh": "ek",
        },
        {
            "region": "sr-Cyrl",
            "lang_sh": "serbian",
            "script_sh": "cyr",
            "baselang": "serbianc",
            "def_pron_sh": "ek",
        },
        {
            "region": "sr-Cyrl-RS",
            "lang_sh": "serbian",
            "script_sh": "cyr",
            "baselang": "serbianc",
            "def_pron_sh": "ij",
        },
        {
            "region": "sr-Cyrl-ME",
            "lang_sh": "serbian",
            "script_sh": "cyr",
            "baselang": "serbianc",
            "def_pron_sh": "ij",
        },
        {
            "region": "sr-Cyrl-BA",
            "lang_sh": "serbian",
            "script_sh": "cyr",
            "baselang": "serbianc",
            "def_pron_sh": "ij",
        },
    ]

    regions_code = "\n".join(generate_region(**region) for region in regions)

    main_dtx_template = latex_env.get_template("main_package_source.dtx")
    main_dtx = main_dtx_template.render(**locals())

    with open("datetime2-serbian.dtx", "w") as dtx:
        dtx.write(main_dtx)

    main_ins_template = latex_env.get_template("main_package_installer.ins")
    main_ins = main_ins_template.render(**locals())

    with open("datetime2-serbian.ins", "w") as ins:
        ins.write(main_ins)


if __name__ == "__main__":
    main()
