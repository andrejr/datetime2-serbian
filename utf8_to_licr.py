#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import subprocess
from typing import Dict
from typing import TextIO


def get_texlive_path(filename: str) -> str:
    """Get a path to a file as recognized by texlive's kpsewhich."""
    # TODO add support for MikTex's miktex-kpsewhich.exe on Windows
    dtx_path_query = subprocess.run(
        ["kpsewhich", filename], stdout=subprocess.PIPE
    )
    return dtx_path_query.stdout.decode("utf-8").splitlines().pop()


def create_translation_table(ienc_dict: TextIO) -> Dict[int, str]:
    """Create a translation table for utf-8 → LICR."""

    pattern = re.compile(r"\DeclareUnicodeCharacter\{([0-9A-Fa-f]+)\}\{(.*)\}")

    with ienc_dict:
        match_groups = (
            pattern.search(line).groups()
            for line in ienc_dict
            if pattern.search(line)
        )

        translation_table = {
            int(codepoint, base=16): latex.replace("@tabacckludge", "")
            for codepoint, latex in match_groups
        }

        if not translation_table:
            raise RuntimeError(
                "The dictionary file provided doesn't contain any valid "
                "character definitions"
            )

        return translation_table


translation_table = create_translation_table(
    open(get_texlive_path("utf8ienc.dtx"), "r", encoding="utf-8")
)
